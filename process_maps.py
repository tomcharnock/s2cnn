import tqdm
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt
import glob

files = sorted(glob.glob("cloud_cover/*.jpg"))

im = plt.imread(files[0], format="jpeg")

n_side = 64
x_size = im.shape[1]
y_size = im.shape[0]
channels = im.shape[2]

long, lat = np.meshgrid(np.linspace(-np.pi, np.pi, x_size), np.linspace(0, np.pi, y_size))
long = long.flatten()
lat = lat.flatten()
pix = hp.ang2pix(n_side, lat, long)

find_indices = False
if find_indices:
    ind = []
    for i in tqdm.tnrange(hp.nside2npix(n_side)):
        potential_ind = np.argwhere(pix==i)
        if len(potential_ind != 0):
            ind.append(potential_ind)
    np.save("cloud_cover/nside64ind.npy", ind)
else:
    ind = np.load("cloud_cover/nside64ind.npy")

maps = np.zeros((len(files), hp.nside2npix(n_side), 3), dtype=np.float32)

for file in tqdm.trange(len(files)):
    im = plt.imread(files[file], format='jpeg')[:, ::-1].reshape((x_size * y_size, 3))
    for pixel in range(12 * n_side**2):
        maps[file, pixel] = np.mean(im[ind[pixel]], 0).astype(np.float32)

np.save("cloud_cover/maps.npy", maps)
